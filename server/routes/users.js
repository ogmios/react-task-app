// users routes
const express = require('express');
const router = express.Router();
const { check } = require('express-validator');

//import controllers
const userController = require('../controllers/userController');

//create user
// api/users

router.post(
  '/',
  [
    check('name', 'name is required').not().isEmpty(),
    check('email').isEmail(),
    check('password', 'password minimun 6 caracthers').isLength({ min: 6 }),
  ],
  userController.createUser
);

module.exports = router;
