const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');
const auth = require('../middlewares/auth');
const { check } = require('express-validator');

router.post(
  '/',
  auth,
  [check('name', 'name project is required').not().isEmpty()],
  [check('project', 'project is required').not().isEmpty()],
  taskController.createTask
);

//get tasks for project
router.get('/:id', auth, taskController.getTasks);

//update project with id
router.put('/:id', auth, taskController.updateTask);

//delete project with id
router.delete('/:id', auth, taskController.deleteTask);

module.exports = router;
