// auth routes
const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const { check } = require('express-validator');

const authController = require('../controllers/authController');

router.post(
  '/',
  [
    check('email', 'Email invalido').isEmail(),
    check(
      'password',
      'El password debe contener como minimo 6 caracteres'
    ).isLength({ min: 6 }),
  ],
  authController.authenticateUser
);

router.get('/', auth, authController.authenticatedUser);

module.exports = router;
