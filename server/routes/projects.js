const express = require('express');
const router = express.Router();
const projectController = require('../controllers/projectController');
const auth = require('../middlewares/auth');
const { check } = require('express-validator');

router.post(
  '/',
  auth,
  [check('name', 'name project is required').not().isEmpty()],
  projectController.createProject
);
//get all projects
router.get('/', auth, projectController.getProjects);

//update project with id
router.put(
  '/:id',
  auth,
  [check('name', 'name project is required').not().isEmpty()],
  projectController.updateProject
);

//delete project with id
router.delete('/:id', auth, projectController.deleteProject);

module.exports = router;
