const User = require('../models/User');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.authenticateUser = async (req, res) => {
  //validate if errors
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  //extract email and password
  const { email, password } = req.body;

  try {
    //check user exist
    let user = await User.findOne({ email });
    if (!user) {
      return res.status(400).json({ msg: 'Usuario no existe' });
    }

    //check pass
    const correctPassword = await bcryptjs.compare(password, user.password);

    if (!correctPassword) {
      return res.status(400).json({ msg: 'Password incorrecto' });
    }

    const payload = {
      user: {
        id: user.id,
      },
    };

    //sign JWT
    jwt.sign(
      payload,
      process.env.SECRET,
      {
        expiresIn: 3600000,
      },
      (error, token) => {
        if (error) throw error;

        //confirm msg
        res.json({ token });
      }
    );
  } catch (error) {}
};

exports.authenticatedUser = async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select('-password');
    res.json({ user });
  } catch (error) {
    res.status(500).json({ msg: 'hubo un error' });
  }
};
