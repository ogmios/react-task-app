const Project = require('../models/Project');
const { validationResult } = require('express-validator');

exports.createProject = async (req, res) => {
  //validate if errors
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    //create new project
    const project = new Project(req.body);
    //save creator with JWT
    project.creator = req.user.id;
    //save project
    project.save();
    res.json(project);
  } catch (error) {
    res.status(500).send('Hubo un error');
  }
};

//get projects
exports.getProjects = async (req, res) => {
  try {
    const projects = await Project.find({ creator: req.user.id }).sort({
      create: -1,
    });
    res.json({ projects });
  } catch (error) {
    res.status(500).send('hubo un error');
  }
};

//update project
exports.updateProject = async (req, res) => {
  //validate if errors
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  //extract info project
  const { name } = req.body;

  const newProject = {};
  if (name) {
    newProject.name = name;
  }

  try {
    //check id
    let project = await Project.findById(req.params.id);
    //check project in collection
    if (!project) {
      return res.status(404).json({ msg: 'Project not found' });
    }
    //check project creator
    if (project.creator.toString() !== req.user.id) {
      res.status(401).json({ msg: 'Not authorizazed' });
    }
    //update
    project = await Project.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: newProject },
      { new: true }
    );

    res.json({ project });
  } catch (error) {
    res.status(500).send('error en el servidor');
  }
};

//delete project
exports.deleteProject = async (req, res) => {
  try {
    //check id
    let project = await Project.findById(req.params.id);
    //check project in collection
    if (!project) {
      return res.status(404).json({ msg: 'Project not found' });
    }
    //check project creator
    if (project.creator.toString() !== req.user.id) {
      res.status(401).json({ msg: 'Not authorizazed' });
    }
    //delete project
    await Project.findOneAndRemove({ _id: req.params.id });
    res.json({ msg: 'project remove' });
  } catch (error) {
    res.status(500).send('error en el servidor');
  }
};
