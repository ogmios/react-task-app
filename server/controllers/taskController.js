const Task = require('../models/Task');
const Project = require('../models/Project');
const { validationResult } = require('express-validator');

exports.createTask = async (req, res) => {
  //validate if errors
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  //extract project and check exist
  try {
    const { project } = req.body;

    const existProject = await Project.findById(project);

    if (!existProject) {
      return res.status(404).json({ msg: 'project not found' });
    }

    //check project creator
    if (existProject.creator.toString() !== req.user.id) {
      res.status(401).json({ msg: 'Not authorizazed' });
    }

    //create task
    const task = new Task(req.body);
    await task.save();
    res.json({ task });
  } catch (error) {
    res.status(500).send('hubo un error');
  }
};

exports.getTasks = async (req, res) => {
  try {
    const project = req.params.id;

    const existProject = await Project.findById(project);

    if (!existProject) {
      return res.status(404).json({ msg: 'project not found' });
    }

    //check project creator
    if (existProject.creator.toString() !== req.user.id) {
      res.status(401).json({ msg: 'Not authorizazed' });
    }

    //get all tasks
    const tasks = await Task.find({ project });

    res.json({ tasks });
  } catch (error) {
    res.status(500).send('hubo un error');
  }
};

exports.updateTask = async (req, res) => {
  try {
    const { project, name, status } = req.body;

    let task = await Task.findById(req.params.id);

    if (!task) {
      return res.status(404).json({ msg: 'Task not found' });
    }

    const existProject = await Project.findById(project);

    //check project creator
    if (existProject.creator.toString() !== req.user.id) {
      res.status(401).json({ msg: 'Not authorizazed' });
    }

    const newTask = {};
    newTask.name = name;
    newTask.status = status;

    // Guardar la tarea
    task = await Task.findOneAndUpdate({ _id: req.params.id }, newTask, {
      new: true,
    });
    res.json({ task });
  } catch (error) {
    res.status(500).send('hubo un problema ');
  }
};

exports.deleteTask = async (req, res) => {
  try {
    const { project } = req.body;

    let task = await Task.findById(req.params.id);

    if (!task) {
      return res.status(404).json({ msg: 'Task not found' });
    }

    const existProject = await Project.findById(project);

    //check project creator
    if (existProject.creator.toString() !== req.user.id) {
      res.status(401).json({ msg: 'Not authorizazed' });
    }

    //delete
    await Task.findOneAndRemove({ _id: req.params.id });
    res.json({ msg: 'delete task' });
  } catch (error) {
    res.status(500).send('hubo un problema ');
  }
};
