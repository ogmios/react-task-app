const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
  //read header token
  const token = req.header('x-auth-token');

  //check if have token
  if (!token) {
    return res.status(401).json({ msg: 'No have token, denied request' });
  }

  //validate token

  try {
    const encryption = jwt.verify(token, process.env.SECRET);
    req.user = encryption.user;
    next();
  } catch (error) {
    res.status(401).json({ msg: 'Invalid token' });
  }
};
