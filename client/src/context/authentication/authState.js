import React, { useReducer } from 'react';
import AuthContext from './authContext';
import AuthReducer from './authReducer';

import clientAxios from '../../config/axios';
import tokenAuth from '../../config/token';

import {
  SUCCESS_REGISTRATION,
  ERROR_REGISTRATION,
  GET_USER,
  SUCCESS_LOGIN,
  ERROR_LOGIN,
  CLOSE_SESION,
} from '../../types';
import axiosClient from '../../config/axios';

const AuthState = (props) => {
  const initialState = {
    token: localStorage.getItem('token'),
    authenticated: null,
    user: null,
    message: null,
    loading: true,
  };

  const [state, dispatch] = useReducer(AuthReducer, initialState);

  const registerUser = async (data) => {
    try {
      const response = await clientAxios.post('/api/users', data);
      dispatch({
        type: SUCCESS_REGISTRATION,
        payload: response.data,
      });

      authenticatedUser();
    } catch (error) {
      const alert = {
        msg: error.response.data.msg,
        category: 'alerta-error',
      };
      dispatch({
        type: ERROR_REGISTRATION,
        payload: alert,
      });
    }
  };

  const authenticatedUser = async () => {
    const token = localStorage.getItem('token');
    if (token) {
      //TODO: send token with header
      tokenAuth(token);
    }

    try {
      const response = await clientAxios.get('/api/auth');
      /* console.log(response); */
      dispatch({
        type: GET_USER,
        payload: response.data.user,
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: ERROR_LOGIN,
      });
    }
  };

  const logIn = async (data) => {
    try {
      const response = await axiosClient.post('/api/auth', data);

      dispatch({
        type: SUCCESS_LOGIN,
        payload: response.data,
      });
      authenticatedUser();
    } catch (error) {
      /* console.log(error.response.data.msg);
      console.log(error.response.data.errors[0].msg); */
      const alert = {
        msg: !error.response.data.msg
          ? error.response.data.errors[0].msg
          : error.response.data.msg,
        category: 'alerta-error',
      };
      dispatch({
        type: ERROR_LOGIN,
        payload: alert,
      });
    }
  };

  const closeSession = () => {
    dispatch({
      type: CLOSE_SESION,
    });
  };

  return (
    <AuthContext.Provider
      value={{
        token: state.token,
        authenticated: state.authenticated,
        user: state.user,
        message: state.message,
        loading: state.loading,
        registerUser,
        logIn,
        authenticatedUser,
        closeSession,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
