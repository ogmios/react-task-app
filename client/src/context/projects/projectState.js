import React, { useReducer } from 'react';

import projectContext from './projectContext';
import projectReducer from './projectReducer';
import {
  PROJECT_FORM,
  GET_PROJECTS,
  ADD_PROJECT,
  VALIDATE_FORM,
  ACTUAL_PROJECT,
  DELETE_PROJECT,
} from '../../types';

import axiosClient from '.././../config/axios';

const ProjectState = (props) => {
  const initialState = {
    projects: [],
    form: false,
    errorForm: false,
    project: null,
  };

  //dispatch for execute actions
  const [state, dispatch] = useReducer(projectReducer, initialState);

  //show side form
  const showForm = () => {
    dispatch({
      type: PROJECT_FORM,
    });
  };

  //get projects
  const getProjects = async () => {
    try {
      const response = await axiosClient.get('/api/projects');

      dispatch({
        type: GET_PROJECTS,
        payload: response.data.projects,
      });
    } catch (error) {
      console.log(error);
    }
  };

  //add new project
  const addProject = async (project) => {
    try {
      const response = await axiosClient.post('/api/projects', project);
      console.log(response);

      dispatch({
        type: ADD_PROJECT,
        payload: response.data,
      });
    } catch (error) {
      console.log(error);
    }
  };

  //validate form errors
  const showError = () => {
    dispatch({
      type: VALIDATE_FORM,
    });
  };

  // select project click
  const actualProject = (projectId) => {
    dispatch({
      type: ACTUAL_PROJECT,
      payload: projectId,
    });
  };

  // delete project click
  const deleteProject = async (projectId) => {
    try {
      await axiosClient.delete(`/api/projects/${projectId}`);
      dispatch({
        type: DELETE_PROJECT,
        payload: projectId,
      });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <projectContext.Provider
      value={{
        projects: state.projects,
        form: state.form,
        errorForm: state.errorForm,
        project: state.project,
        showForm,
        getProjects,
        addProject,
        showError,
        actualProject,
        deleteProject,
      }}
    >
      {props.children}
    </projectContext.Provider>
  );
};

export default ProjectState;
