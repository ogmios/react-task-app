import React, { Fragment, useContext } from 'react';
import Task from './Task';
import projectContext from '../../context/projects/projectContext';

const ListTasks = () => {
  const projectsContext = useContext(projectContext);
  const { project, deleteProject } = projectsContext;

  //not selected project
  if (!project) return <h2>Selecciona un proyecto</h2>;

  //array desctructuring
  const [actualProject] = project;

  const tasks = [
    { id: 1, name: 'Elegir plataforma', status: true },
    { id: 2, name: 'Elegir colores', status: false },
    { id: 3, name: 'Elegir plataforma de pago', status: false },
    { id: 4, name: 'Elegir hosting', status: true },
  ];

  const onClickDelete = () => {
    deleteProject(actualProject._id);
  };

  return (
    <Fragment>
      <h2>Proyecto: {actualProject.name}</h2>
      <ul className='listado-tareas'>
        {tasks.length === 0 ? (
          <li className='tarea'>
            <p>No hay tareas</p>
          </li>
        ) : (
          tasks.map((task) => <Task task={task} key={task.id} />)
        )}
        <button
          type='button'
          className='btn btn-eliminar'
          onClick={onClickDelete}
        >
          Eliminar proyecto &times;
        </button>
      </ul>
    </Fragment>
  );
};

export default ListTasks;
