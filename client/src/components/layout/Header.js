import React, { useContext, useEffect } from 'react';
import AuthContext from '../../context/authentication/authContext';

const Header = () => {
  const authContext = useContext(AuthContext);
  const { user, authenticatedUser, closeSession } = authContext;

  useEffect(() => {
    authenticatedUser();
  }, []);

  return (
    <header className='app-header'>
      {user && (
        <p className='nombre-usuario'>
          Hola <span>{user.name}</span>
        </p>
      )}
      <nav className='nav-principal'>
        <button
          className='btn btn-blank cerrar-sesion'
          onClick={() => closeSession()}
        >
          Cerrar sesión
        </button>
      </nav>
    </header>
  );
};

export default Header;
