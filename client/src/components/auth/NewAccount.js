import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AlertaContext from '../../context/alerts/alertContext';
import AuthContext from '../../context/authentication/authContext';

const NewAccount = (props) => {
  const alertaContext = useContext(AlertaContext);
  const { alert, showAlert } = alertaContext;

  const authContext = useContext(AuthContext);
  const { message, authenticated, registerUser } = authContext;

  useEffect(() => {
    if (authenticated) {
      props.history.push('/projects');
    }

    if (message) {
      showAlert(message.msg, message.category);
    }
  }, [message, authenticated, props.history]);

  //state init session
  const [user, setUser] = useState({
    name: '',
    email: '',
    password: '',
    confirm: '',
  });

  //extract email and password from user
  const { name, email, password, confirm } = user;

  const onChange = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };

  //submit form
  const onSubmit = (e) => {
    e.preventDefault();

    //validate empty fields
    if (
      name.trim() === '' ||
      email.trim() === '' ||
      password.trim() === '' ||
      confirm.trim() === ''
    ) {
      showAlert('Todos los campos son obligatorios', 'alerta-error');
      return;
    }

    //pass minimum 6 characters
    if (password.length < 6) {
      showAlert(
        'El password debe ser de al menos 6 caracteres',
        'alerta-error'
      );
      return;
    }

    if (password !== confirm) {
      showAlert('Los password no son iguales', 'alerta-error');
      return;
    }

    registerUser({
      name,
      email,
      password,
    });
  };

  return (
    <div className='form-usuario'>
      {alert && <div className={`alerta ${alert.category}`}> {alert.msg} </div>}
      <div className='contenedor-form sombra-dark'>
        <h1>Obtener una cuenta</h1>
        <form onSubmit={onSubmit}>
          <div className='campo-form'>
            <label htmlFor='name'>Nombre</label>
            <input
              type='text'
              id='name'
              name='name'
              placeholder='Tu nombre'
              value={name}
              onChange={onChange}
            />
          </div>

          <div className='campo-form'>
            <label htmlFor='email'>Email</label>
            <input
              type='email'
              id='email'
              name='email'
              placeholder='Tu Email'
              value={email}
              onChange={onChange}
            />
          </div>

          <div className='campo-form'>
            <label htmlFor='password'>Password</label>
            <input
              type='password'
              id='password'
              name='password'
              placeholder='Tu Password'
              value={password}
              onChange={onChange}
            />
          </div>

          <div className='campo-form'>
            <label htmlFor='password'>Confirmar password</label>
            <input
              type='password'
              id='confirm'
              name='confirm'
              placeholder='Repite tu password'
              value={confirm}
              onChange={onChange}
            />
          </div>

          <div className='campo-form'>
            <input
              type='submit'
              className='btn btn-primario btn-block'
              value='Registrarme'
            />
          </div>
        </form>
        <Link to={'/'} className='enlace-cuenta'>
          Volver
        </Link>
      </div>
    </div>
  );
};

export default NewAccount;
