import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AlertaContext from '../../context/alerts/alertContext';
import AuthContext from '../../context/authentication/authContext';

const Login = (props) => {
  const alertaContext = useContext(AlertaContext);
  const { alert, showAlert } = alertaContext;

  const authContext = useContext(AuthContext);
  const { message, authenticated, logIn } = authContext;

  useEffect(() => {
    if (authenticated) {
      props.history.push('/projects');
    }

    if (message) {
      showAlert(message.msg, message.category);
    }
  }, [message, authenticated, props.history]);
  //state init session
  const [user, setUser] = useState({
    email: '',
    password: '',
  });

  //extract email and password from user
  const { email, password } = user;

  const onChange = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };

  //submit form
  const onSubmit = (e) => {
    e.preventDefault();

    //validate empty fields
    if (email.trim() === '' || password.trim() === '') {
      showAlert('Todos los campos son obligatorios.', 'alerta-error');
      return;
    }

    //pass to action
    logIn({ email, password });
  };

  return (
    <div className='form-usuario'>
      {alert && <div className={`alerta ${alert.category}`}> {alert.msg} </div>}
      <div className='contenedor-form sombra-dark'>
        <h1>Iniciar sesión</h1>
        <form onSubmit={onSubmit}>
          <div className='campo-form'>
            <label htmlFor='email'>Email</label>
            <input
              type='email'
              id='email'
              name='email'
              placeholder='Tu Email'
              value={email}
              onChange={onChange}
            />
          </div>

          <div className='campo-form'>
            <label htmlFor='password'>Password</label>
            <input
              type='password'
              id='password'
              name='password'
              placeholder='Tu Password'
              value={password}
              onChange={onChange}
            />
          </div>
          <div className='campo-form'>
            <input
              type='submit'
              className='btn btn-primario btn-block'
              value='Iniciar sesión'
            />
          </div>
        </form>
        <Link to={'/new-account'} className='enlace-cuenta'>
          Obtener cuenta
        </Link>
      </div>
    </div>
  );
};

export default Login;
