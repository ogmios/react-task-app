import React, { useContext, useEffect } from 'react';

import Project from './Project';
import projectContext from '../../context/projects/projectContext';

const ListProjects = () => {
  //get projects state from context
  const projectsContext = useContext(projectContext);
  const { projects, getProjects } = projectsContext;

  //get project when component mount
  useEffect(() => {
    getProjects();
  }, []);

  //check project is not empty
  if (projects.length === 0) return <p>No hay proyectos</p>;

  return (
    <ul className='listado-proyectos'>
      {projects.map((project) => (
        <Project project={project} key={project._id} />
      ))}
    </ul>
  );
};

export default ListProjects;
