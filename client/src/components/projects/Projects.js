import React, { useContext, useEffect } from 'react';
import Sidebar from '../layout/Sidebar';
import Header from '../layout/Header';
import TaskForm from '../tasks/TaskForm';
import ListTasks from '../tasks/ListTasks';
import AuthContext from '../../context/authentication/authContext';

const Projects = () => {
  //extract auth info
  const authContext = useContext(AuthContext);
  const { authenticatedUser } = authContext;

  useEffect(() => {
    authenticatedUser();
  }, []);

  return (
    <div className='contenedor-app'>
      <Sidebar />
      <div className='seccion-principal'>
        <Header />
        <main>
          <TaskForm />
          <div className='contenedor-tareas'>
            <ListTasks />
          </div>
        </main>
      </div>
    </div>
  );
};

export default Projects;
