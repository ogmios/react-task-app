import React, { Fragment, useState, useContext } from 'react';
import projectContext from '../../context/projects/projectContext';

const NewProject = () => {
  //get form state from context
  const projectsContext = useContext(projectContext);
  const { form, errorForm, showForm, addProject, showError } = projectsContext;

  //state for project
  const [project, setProject] = useState({
    name: '',
  });

  //extract name project from project
  const { name } = project;

  //set inputs content
  const onChangeProject = (e) => {
    setProject({
      ...project,
      [e.target.name]: e.target.value,
    });
  };

  //when user send project
  const onSubmitProject = (e) => {
    e.preventDefault();

    //validate project
    if (name === '') {
      showError();
      return;
    }

    //add state
    addProject(project);

    //restart form
    setProject({
      name: '',
    });
  };

  const onClickForm = () => {
    showForm();
  };

  return (
    <Fragment>
      <button
        type='button'
        className='btn btn-block btn-primario'
        onClick={onClickForm}
      >
        Nuevo proyecto
      </button>
      {form && (
        <form className='formulario-nuevo-proyecto' onSubmit={onSubmitProject}>
          <input
            type='text'
            className='input-text'
            placeholder='Nombre proyecto'
            name='name'
            value={name}
            onChange={onChangeProject}
          />
          <input
            type='submit'
            className='btn btn-primario btn-block'
            value='Agregar proyecto'
          />
        </form>
      )}
      {errorForm && (
        <p className='mensaje error'>El nombre del proyecto es obligatorio</p>
      )}
    </Fragment>
  );
};

export default NewProject;
