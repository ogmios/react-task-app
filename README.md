## React Task App

#### Descripción:

Proyecto para la gestión de tareas colaborativa, se puede crear una cuenta, logear con la cuenta, crear proyectos, y crear tareas, más el CRUD correspondiente de ambos módulos.

## Instalación

Primero hay que clonar este repositorio. Es necesario tener instalado `node` and `npm` or `yarn` de manera global en su maquina.

Ir a la carpeta client y server, en cada una aplicar los siguientes comandos:

Instalación:

`npm install` o `yarn`

Ejecutar App:

`npm start` o `yarn start`

Visitar App:

`localhost:3000`

#### Conclusión:

Este proyecto ha tomado hasta ahora 3 dias en su construcción, y esta siendo realizado para poder ser una guía en el desarrollo con React y Node.

Este proyecto fue realizado con `create-react-app`.
